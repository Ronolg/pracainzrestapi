package com.gainIt.restapi.service;

import com.gainIt.restapi.model.Workout;

import java.util.List;

public interface WorkoutService {
    void addWorkoutForUser(Long userId, Workout workout);
    List<Workout> getAllWorkouts(Long userId);
    Workout getWorkoutById(Long userId, Long workoutId);
    void deleteWorkoutById(Long userId, Long workoutId);
}

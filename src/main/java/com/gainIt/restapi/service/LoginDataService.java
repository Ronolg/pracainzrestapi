package com.gainIt.restapi.service;

import com.gainIt.restapi.model.LoginData;

public interface LoginDataService {
    public void updateUserLoginData(Long userId, LoginData loginData);
}

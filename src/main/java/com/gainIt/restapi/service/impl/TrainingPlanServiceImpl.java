package com.gainIt.restapi.service.impl;


import com.gainIt.restapi.exception.UserNotFoundException;
import com.gainIt.restapi.exception.TrainingPlanNotFoundException;
import com.gainIt.restapi.model.*;
import com.gainIt.restapi.repository.ExerciseRepository;
import com.gainIt.restapi.service.TrainingPlanService;
import com.gainIt.restapi.repository.TrainingPlanRepository;
import com.gainIt.restapi.repository.UserRepository;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class TrainingPlanServiceImpl implements TrainingPlanService {

    private final UserRepository userRepository;
    private final TrainingPlanRepository trainingPlanRepository;

    public TrainingPlanServiceImpl(UserRepository userRepository, TrainingPlanRepository trainingPlanRepository, ExerciseRepository exerciseRepository) {
        this.userRepository = userRepository;
        this.trainingPlanRepository = trainingPlanRepository;
    }

    @Override
    public void addTrainingPlan(Long userId, TrainingPlan trainingPlan) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found with id: " + userId));

        user.addTrainingPlan(trainingPlan);
        trainingPlanRepository.save(trainingPlan);
    }

    @Override
    public void addCurrentTrainingPlan(Long userId, TrainingPlan trainingPlan) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found with id: " + userId));

        trainingPlanRepository.resetPreviousCurrentTrainingPlan(userId);
        trainingPlan.setCurrent(true);
        user.addTrainingPlan(trainingPlan);
        trainingPlanRepository.save(trainingPlan);
    }

    @Override
    public void updateTrainingPlanForUser(Long userId, Long trainingPlanId, TrainingPlan trainingPlan) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found with id: " + userId));

        TrainingPlan updatedTrainingPlan = trainingPlanRepository.findById(trainingPlanId)
                .orElseThrow(() -> new TrainingPlanNotFoundException("Training plan not found with id: " + userId));

        updatedTrainingPlan.setId(trainingPlanId);
        updatedTrainingPlan.setUser(user);
        user.updateTrainingPlan(updatedTrainingPlan);

        trainingPlanRepository.save(updatedTrainingPlan);
    }

    @Override
    public TrainingPlan getCurrentTrainingPlanById(Long userId) {
        userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found with id: " + userId));

        return trainingPlanRepository.getCurrentTrainingPlan(userId)
                .orElseThrow(() -> new TrainingPlanNotFoundException("Training plan not found for user: " + userId));
    }

    @Override
    public TrainingPlan getTrainingPlanById(Long userId, Long trainingPlanId) {
        userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found with id: " + userId));

        return trainingPlanRepository.findById(trainingPlanId)
                .orElseThrow(() -> new TrainingPlanNotFoundException("Training plan not found for user: " + userId));
    }

    @Override
    public List<TrainingPlan> getAllTrainingPlans(Long userId) {
        return trainingPlanRepository.findAllForUser(userId);
    }

    @Override
    public void deleteTrainingPlanById(Long userId, Long trainingPlanId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found with id: " + userId));

        TrainingPlan trainingPlan = trainingPlanRepository.findById(trainingPlanId).
                orElseThrow(() -> new TrainingPlanNotFoundException("Training plan not found with id: " + trainingPlanId));

        user.removeTrainingPlan(trainingPlan);
        trainingPlanRepository.deleteById(trainingPlanId);
    }
}

package com.gainIt.restapi.service.impl;

import com.gainIt.restapi.exception.UserNotFoundException;
import com.gainIt.restapi.model.LoginData;
import com.gainIt.restapi.model.User;
import com.gainIt.restapi.repository.LoginDataRepository;
import com.gainIt.restapi.repository.UserRepository;
import com.gainIt.restapi.service.LoginDataService;
import org.springframework.stereotype.Service;

@Service
public class LoginDataServiceImpl implements LoginDataService {
    private final LoginDataRepository loginDataRepository;
    private final UserRepository userRepository;

    public LoginDataServiceImpl(LoginDataRepository loginDataRepository, UserRepository userRepository) {
        this.loginDataRepository = loginDataRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void updateUserLoginData(Long userId, LoginData loginData) {
        User user  = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found with id: " + userId));

        loginData.setUser(user);
        loginDataRepository.save(loginData);
    }
}

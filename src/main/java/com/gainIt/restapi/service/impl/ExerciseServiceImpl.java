package com.gainIt.restapi.service.impl;

import com.gainIt.restapi.dto.ExerciseDTO;
import com.gainIt.restapi.model.Exercise;
import com.gainIt.restapi.repository.ExerciseRepository;
import com.gainIt.restapi.service.ExerciseService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExerciseServiceImpl implements ExerciseService {

    private final ExerciseRepository exerciseRepository;

    public ExerciseServiceImpl(ExerciseRepository exerciseRepository) {
        this.exerciseRepository = exerciseRepository;
    }

    @Override
    public void addExercise(Exercise exercise) {
        exerciseRepository.save(exercise);
    }

    @Override
    public void addExercises(List<Exercise> exercisesList) {
        for(Exercise exercise : exercisesList) {
            exerciseRepository.save(exercise);
        }
    }

    @Override
    public List<ExerciseDTO> getAllExercises() {
        List<Exercise> exercises = exerciseRepository.findAll();
        List<ExerciseDTO> exerciseDTOs = new ArrayList<>();

        exercises.forEach((exercise) -> {
            ExerciseDTO exerciseDTO = new ExerciseDTO(exercise);
            exerciseDTOs.add(exerciseDTO);
        });

        return exerciseDTOs;
    }

    @Override
    public List<ExerciseDTO> getAllExercisesVerifiedOnly() {
        List<Exercise> exercisesVerifiedOnly =  exerciseRepository.findAllVerifiedOnly();
        List<ExerciseDTO> exerciseDTOs = new ArrayList<>();

        exercisesVerifiedOnly.forEach((exercise) -> {
            ExerciseDTO exerciseDTO = new ExerciseDTO(exercise);
            exerciseDTOs.add(exerciseDTO);
        });

        return exerciseDTOs;
    }
}

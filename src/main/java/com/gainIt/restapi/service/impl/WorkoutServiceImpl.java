package com.gainIt.restapi.service.impl;

import com.gainIt.restapi.exception.UserNotFoundException;
import com.gainIt.restapi.exception.WorkoutNotFoundException;
import com.gainIt.restapi.model.*;
import com.gainIt.restapi.repository.ExerciseRepository;
import com.gainIt.restapi.repository.UserRepository;
import com.gainIt.restapi.repository.WorkoutRepository;
import com.gainIt.restapi.service.WorkoutService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WorkoutServiceImpl implements WorkoutService {

    private final UserRepository userRepository;
    private final WorkoutRepository workoutRepository;

    public WorkoutServiceImpl(UserRepository userRepository, WorkoutRepository workoutRepository, ExerciseRepository exerciseRepository) {
        this.userRepository = userRepository;
        this.workoutRepository = workoutRepository;
    }

    @Override
    public void addWorkoutForUser(Long userId, Workout workout) {
        Optional<User> optionalUser = userRepository.findById(userId);

        List<UserExercise> userExerciseList = workout.getUserExercises();

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();

            user.addWorkout(workout);
            workoutRepository.save(workout);
        } else {
            throw new IllegalArgumentException("User not found with id: " + userId);
        }
    }

    @Override
    public List<Workout> getAllWorkouts(Long userId) {
        return workoutRepository.findAllForUser(userId);
    }

    @Override
    public Workout getWorkoutById(Long userId, Long workoutId) {

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found with id: " + userId));

        return workoutRepository.findById(workoutId)
                .orElseThrow(() -> new WorkoutNotFoundException("Workout not found with id:" + workoutId));
    }


    @Override
    public void deleteWorkoutById(Long userId, Long workoutId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found with id: " + userId));

        Optional<Workout> workout = Optional.ofNullable(workoutRepository.findById(workoutId).
                orElseThrow(() -> new WorkoutNotFoundException("Workout not found with id: " + workoutId)));

        if (workout.isPresent()) {
            user.removeWorkout(workout.get());
            workoutRepository.deleteById(workoutId);
        }
    }
}

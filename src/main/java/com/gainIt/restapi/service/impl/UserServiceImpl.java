package com.gainIt.restapi.service.impl;

import com.gainIt.restapi.exception.UserNotFoundException;
import com.gainIt.restapi.model.LoginData;
import com.gainIt.restapi.model.User;
import com.gainIt.restapi.repository.LoginDataRepository;
import com.gainIt.restapi.repository.UserRepository;
import com.gainIt.restapi.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final LoginDataRepository loginDataRepository;

    public UserServiceImpl(UserRepository userRepository, LoginDataRepository loginDataRepository) {
        this.userRepository = userRepository;
        this.loginDataRepository = loginDataRepository;
    }

    @Override
    public User addUserWithLoginData(LoginData loginData) {
        User user = new User();
        user.setLoginData(loginData);
        userRepository.save(user);
        loginDataRepository.save(loginData);

        return user;
    }

    @Override
    public User deleteUserById(Long userId) {
        loginDataRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found with id: " + userId));
        loginDataRepository.deleteById(userId);

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found with id: " + userId));
        userRepository.deleteById(userId);

        return user;
    }
}

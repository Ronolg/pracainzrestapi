package com.gainIt.restapi.service.impl;

import com.gainIt.restapi.model.Exercise;
import com.gainIt.restapi.model.Set;
import com.gainIt.restapi.model.UserExercise;
import com.gainIt.restapi.repository.UserExerciseRepository;
import com.gainIt.restapi.service.UserExerciseService;
import org.springframework.stereotype.Service;

@Service
public class UserExerciseServiceImpl implements UserExerciseService {

    private final UserExerciseRepository userExerciseRepository;

    public UserExerciseServiceImpl(UserExerciseRepository userExerciseRepository) {
        this.userExerciseRepository = userExerciseRepository;
    }

    @Override
    public UserExercise addUserExercise(UserExercise userExercise, Exercise exercise) {
        for (Set set : userExercise.getSets()) {
            set.setUserExercise(userExercise);
        }
        return userExerciseRepository.save(userExercise);
    }
}

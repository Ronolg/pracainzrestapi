package com.gainIt.restapi.service;

import com.gainIt.restapi.dto.ExerciseDTO;
import com.gainIt.restapi.model.Exercise;

import java.util.List;

public interface ExerciseService {
    public void addExercise(Exercise exercise);
    public void addExercises(List<Exercise> exercisesList);
    public List<ExerciseDTO> getAllExercises();
    public List<ExerciseDTO> getAllExercisesVerifiedOnly();

}

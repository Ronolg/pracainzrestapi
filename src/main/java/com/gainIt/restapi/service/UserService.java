package com.gainIt.restapi.service;

import com.gainIt.restapi.model.LoginData;
import com.gainIt.restapi.model.User;

public interface UserService {
    public User addUserWithLoginData(LoginData loginData);

    public User deleteUserById(Long userId);
}

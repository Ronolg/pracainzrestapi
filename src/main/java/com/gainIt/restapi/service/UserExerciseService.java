package com.gainIt.restapi.service;

import com.gainIt.restapi.model.Exercise;
import com.gainIt.restapi.model.UserExercise;

public interface UserExerciseService {
    public UserExercise addUserExercise(UserExercise userExercise, Exercise exercise);
}

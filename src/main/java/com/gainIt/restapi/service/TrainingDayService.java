package com.gainIt.restapi.service;

import com.gainIt.restapi.model.TrainingDay;

public interface TrainingDayService {
    public TrainingDay addTrainingDay(TrainingDay trainingDay);
}

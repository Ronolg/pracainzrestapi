package com.gainIt.restapi.service;

import com.gainIt.restapi.model.TrainingPlan;

import java.util.List;
import java.util.Optional;

public interface TrainingPlanService {
    public void addTrainingPlan(Long userId, TrainingPlan trainingPlan);
    public void addCurrentTrainingPlan(Long userId, TrainingPlan trainingPlan);
    public void updateTrainingPlanForUser(Long userId, Long trainingPlanId, TrainingPlan trainingPlan);
    public TrainingPlan getCurrentTrainingPlanById(Long userId);
    public TrainingPlan getTrainingPlanById(Long userId, Long trainingPlanId);
    public List<TrainingPlan> getAllTrainingPlans(Long userId);
    public void deleteTrainingPlanById(Long userId, Long trainingPlanId);
}

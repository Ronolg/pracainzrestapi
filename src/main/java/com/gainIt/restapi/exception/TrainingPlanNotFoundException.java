package com.gainIt.restapi.exception;

public class TrainingPlanNotFoundException extends RuntimeException {
    public TrainingPlanNotFoundException(String message) {
        super(message);
    }
}

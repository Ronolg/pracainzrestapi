package com.gainIt.restapi.repository;

import com.gainIt.restapi.model.UserExercise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserExerciseRepository extends JpaRepository<UserExercise, Long> {
}

package com.gainIt.restapi.repository;

import com.gainIt.restapi.model.Exercise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ExerciseRepository extends JpaRepository<Exercise, Long> {
    @Query(value = "SELECT * FROM exercises WHERE verified = 1", nativeQuery = true)
    List<Exercise> findAllVerifiedOnly();

    @Query("SELECT e FROM Exercise e WHERE e.name = :name")
    Optional<Exercise> findByName(@Param("name") String name);
}

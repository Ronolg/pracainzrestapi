package com.gainIt.restapi.repository;

import com.gainIt.restapi.model.Workout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkoutRepository extends JpaRepository<Workout, Long> {
    @Query(value = "SELECT * FROM workouts WHERE userID = :userID", nativeQuery = true)
    List<Workout> findAllForUser(@Param("userID") Long userId);
}

package com.gainIt.restapi.repository;

import com.gainIt.restapi.model.TrainingPlan;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TrainingPlanRepository extends JpaRepository<TrainingPlan, Long> {
    @Transactional
    @Modifying
    @Query(value = "UPDATE trainingplans SET isCurrent = 0 WHERE isCurrent = 1 AND userID = :userID", nativeQuery = true)
    void resetPreviousCurrentTrainingPlan(@Param("userID") Long userId);

    @Query(value = "SELECT * FROM trainingplans WHERE userID = :userID AND isCurrent = 1", nativeQuery = true)
    Optional<TrainingPlan> getCurrentTrainingPlan(@Param("userID") Long userId);

    @Query(value = "SELECT * FROM trainingplans WHERE userID = :userID", nativeQuery = true)
    List<TrainingPlan> findAllForUser(@Param("userID") Long userId);
}

package com.gainIt.restapi.controller;

import com.gainIt.restapi.dto.ExerciseDTO;
import com.gainIt.restapi.model.Exercise;
import com.gainIt.restapi.service.ExerciseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/exercise")
public class ExerciseController {

    private final ExerciseService exerciseService;

    public ExerciseController(ExerciseService exerciseService) {
        this.exerciseService = exerciseService;
    }

    @PostMapping("/add")
    public ResponseEntity<Object> addExercise(@RequestBody Exercise exercise) {
        try {
            exerciseService.addExercise(exercise);
            return ResponseEntity.status(HttpStatus.CREATED).body("Exercise created successfully");
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to create exercise", e);
        }
    }

    @PostMapping("/addExercises")
    public ResponseEntity<Object> addExercises(@RequestBody List<Exercise> exercises) {
        try {
            exerciseService.addExercises(exercises);
            return ResponseEntity.status(HttpStatus.CREATED).body("Exercises created successfully");
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to create exercises", e);
        }
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<ExerciseDTO>> getAllExercises() {
        try {
            List<ExerciseDTO> exercises = exerciseService.getAllExercises();
            if (exercises.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(exercises, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/getAllVerified")
    public ResponseEntity<List<ExerciseDTO>> getAllExercisesVerifiedOnly() {
        try {
            List<ExerciseDTO> exercises = exerciseService.getAllExercisesVerifiedOnly();
            if (exercises.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(exercises, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

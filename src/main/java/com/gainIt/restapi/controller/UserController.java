package com.gainIt.restapi.controller;

import com.gainIt.restapi.exception.ExerciseNotFoundException;
import com.gainIt.restapi.exception.TrainingPlanNotFoundException;
import com.gainIt.restapi.exception.UserNotFoundException;
import com.gainIt.restapi.exception.WorkoutNotFoundException;
import com.gainIt.restapi.model.*;
import com.gainIt.restapi.service.LoginDataService;
import com.gainIt.restapi.service.TrainingPlanService;
import com.gainIt.restapi.service.UserService;
import com.gainIt.restapi.service.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@RequestMapping("/user")
public class UserController {
    private final UserService userService;
    private final LoginDataService loginDataService;
    private final TrainingPlanService trainingPlanService;
    private final WorkoutService workoutService;

    @Autowired
    public UserController(UserService userService, LoginDataService loginDataService, TrainingPlanService trainingPlanService, WorkoutService workoutService) {
        this.userService = userService;
        this.loginDataService = loginDataService;
        this.trainingPlanService = trainingPlanService;
        this.workoutService = workoutService;
    }

    // USER METHODS
    @PostMapping("/add")
    public ResponseEntity<Object> addUserWithLoginData(@RequestBody LoginData loginData) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(userService.addUserWithLoginData(loginData));
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to create user", e);
        }
    }

    @PutMapping("/{userId}/loginData/update")
    public ResponseEntity<String> updateUserLoginData(@PathVariable Long userId, @RequestBody LoginData loginData) {
        try {
            loginDataService.updateUserLoginData(userId, loginData);
            return ResponseEntity.status(HttpStatus.OK).body("User credentials were updated successfully");
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Failed to update user's credentials", e);
        }
    }

    @DeleteMapping("{id}/delete/")
    public ResponseEntity<User> deleteUser(@PathVariable("id") Long userId) {
        try {
            User deletedUser = userService.deleteUserById(userId);
            return ResponseEntity.status(HttpStatus.OK).body(deletedUser);
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Failed to delete User", e);
        }
    }

    // TRAINING PLAN METHODS
    @PostMapping("/{userId}/trainingPlan/add")
    public ResponseEntity<String> addTrainingPlanToUser(@PathVariable Long userId, @RequestBody TrainingPlan trainingPlan) {
        try {
            trainingPlanService.addTrainingPlan(userId, trainingPlan);
            return new ResponseEntity<>("Training plan added to the user successfully", HttpStatus.CREATED);
        } catch (ExerciseNotFoundException | UserNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/{userId}/trainingPlan/add/current")
    public ResponseEntity<String> addCurrentTrainingPlanToUser(@PathVariable Long userId, @RequestBody TrainingPlan trainingPlan) {
        try {
            trainingPlanService.addCurrentTrainingPlan(userId, trainingPlan);
            return new ResponseEntity<>("Current training plan added to the user successfully", HttpStatus.CREATED);
        } catch (ExerciseNotFoundException | UserNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("{userId}/trainingPlan/update/{trainingPlanId}")
    public ResponseEntity<String> updateTrainingPlan(@PathVariable Long userId, @PathVariable Long trainingPlanId, @RequestBody TrainingPlan trainingPlan) {
        try {
            trainingPlanService.updateTrainingPlanForUser(userId, trainingPlanId, trainingPlan);
            return ResponseEntity.status(HttpStatus.OK).body("Training plan updated successfully");
        } catch (UserNotFoundException | TrainingPlanNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Failed to update the training plan", e);
        }
    }

    @GetMapping("/{userId}/trainingPlan/get/current")
    public ResponseEntity<Object> getCurrentTrainingPlan(@PathVariable Long userId) {
        try {
            TrainingPlan currentTrainingPlan = trainingPlanService.getCurrentTrainingPlanById(userId);
            return ResponseEntity.status(HttpStatus.OK).body(currentTrainingPlan);
        } catch (UserNotFoundException | TrainingPlanNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{userId}/trainingPlan/get/{trainingPlanId}")
    public ResponseEntity<Object> getTrainingPlanById(@PathVariable Long userId, @PathVariable Long trainingPlanId) {
        try {
            TrainingPlan currentTrainingPlan = trainingPlanService.getTrainingPlanById(userId, trainingPlanId);
            return ResponseEntity.status(HttpStatus.OK).body(currentTrainingPlan);
        } catch (UserNotFoundException | TrainingPlanNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{userId}/trainingPlan/get/all")
    public ResponseEntity<List<TrainingPlan>> getAllTrainingPlansForUser(@PathVariable Long userId) {
        try {
            List<TrainingPlan> trainingPlans = trainingPlanService.getAllTrainingPlans(userId);
            if (trainingPlans.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(trainingPlans, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{userId}/deleteTrainingPlan/{trainingPlanId}")
    public ResponseEntity<Object> deleteTrainingPlan(@PathVariable Long userId, @PathVariable Long trainingPlanId) {
        try {
            trainingPlanService.deleteTrainingPlanById(userId, trainingPlanId);
            return new ResponseEntity<>("Training plan was deleted successfully", HttpStatus.OK);
        } catch (UserNotFoundException | TrainingPlanNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    // WORKOUT METHODS
    @PostMapping("/{userId}/addWorkout")
    public ResponseEntity<Object> addWorkout(@PathVariable Long userId, @RequestBody Workout workout) {
        try {
            workoutService.addWorkoutForUser(userId, workout);
            return new ResponseEntity<>("Workout was added to the user successfully", HttpStatus.CREATED);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{userId}/getWorkout/all")
    public ResponseEntity<List<Workout>> getAllWorkoutsForUser(@PathVariable Long userId) {
        try {
            List<Workout> workouts = workoutService.getAllWorkouts(userId);
            if (workouts.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(workouts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{userId}/getWorkout/{workoutId}")
    public ResponseEntity<Object> getWorkoutByIdForUser(@PathVariable Long userId, @PathVariable Long workoutId) {
        try {
            Workout workout = workoutService.getWorkoutById(userId, workoutId);
            return new ResponseEntity<>(workout, HttpStatus.OK);
        } catch (UserNotFoundException | WorkoutNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{userId}/deleteWorkout/{workoutId}")
    public ResponseEntity<Object> deleteWorkout(@PathVariable Long userId, @PathVariable Long workoutId) {
        try {
            workoutService.deleteWorkoutById(userId, workoutId);
            return new ResponseEntity<>("Workout was deleted successfully", HttpStatus.OK);
        } catch (UserNotFoundException | WorkoutNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }
}

package com.gainIt.restapi.dto;

import com.gainIt.restapi.model.Exercise;

public class ExerciseDTO {
    private String name;
    private String description;

    public ExerciseDTO(Exercise exercise) {
        this.name = exercise.getName();
        this.description = exercise.getDescription();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

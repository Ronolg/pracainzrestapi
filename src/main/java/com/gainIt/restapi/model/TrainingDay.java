package com.gainIt.restapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "trainingdays")
public class TrainingDay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trainingPlanID")
    private TrainingPlan trainingPlan;

    @OneToMany(mappedBy = "trainingDay", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserExercise> userExercises;

    public TrainingDay() {
    }

    public TrainingDay(TrainingPlan trainingPlan) {
        this.trainingPlan = trainingPlan;
    }

    public Long getId() {
        return id;
    }

    public TrainingPlan getTrainingPlan() {
        return trainingPlan;
    }

    public List<UserExercise> getUserExercises() {
        return userExercises;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTrainingPlan(TrainingPlan trainingPlan) {
        this.trainingPlan = trainingPlan;
    }

    public void setUserExercises(List<UserExercise> userExercises) {
        this.userExercises = userExercises;
    }

    public void addUserExercise(UserExercise userExercise) {
        userExercises.add(userExercise);
        userExercise.setTrainingDay(this);
    }

    public void removeUserExercise(UserExercise userExercise) {
        userExercises.remove(userExercise);
        userExercise.setTrainingDay(null);
    }

    public void setUserExercisesReferences() {
        for(UserExercise userExercise : userExercises) {
            userExercise.setTrainingDay(this);
            userExercise.setSetsReferences();
        }
    }
}

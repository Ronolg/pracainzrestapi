package com.gainIt.restapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "trainingplans")
public class TrainingPlan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    private boolean isCurrent;
    private String name;
    private String note;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userID")
    private User user;

    @OneToMany(mappedBy = "trainingPlan", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TrainingDay> trainingDays = new ArrayList<>();

    public TrainingPlan() {
    }

    public TrainingPlan(String name, String note, List<TrainingDay> trainingDays) {
        this.isCurrent = false;
        this.name = name;
        this.note = note;
        this.trainingDays = trainingDays;
    }

    public TrainingPlan(boolean isCurrent, String name, String note, List<TrainingDay> trainingDays) {
        this.isCurrent = isCurrent;
        this.name = name;
        this.note = note;
        this.trainingDays = trainingDays;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public boolean isCurrent() {
        return isCurrent;
    }

    public String getName() {
        return name;
    }

    public String getNote() {
        return note;
    }

    public List<TrainingDay> getTrainingDays() {
        return trainingDays;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setCurrent(boolean current) {
        isCurrent = current;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setTrainingDays(List<TrainingDay> trainingDays) {
        this.trainingDays = trainingDays;
    }

    public void addTrainingDay(TrainingDay trainingDay) {
        trainingDays.add(trainingDay);
        trainingDay.setTrainingPlan(this);
    }

    public void removeTrainingDay(TrainingDay trainingDay) {
        trainingDays.remove(trainingDay);
        trainingDay.setTrainingPlan(null);
    }

    public void setTrainingDaysReferences() {
        for(TrainingDay trainingDay : trainingDays) {
            trainingDay.setTrainingPlan(this);
            trainingDay.setUserExercisesReferences();
        }
    }
}

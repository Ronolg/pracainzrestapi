package com.gainIt.restapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "workouts")
public class Workout {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    private Date date;
    private String note;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userID")
    private User user;

    @OneToMany(mappedBy = "workout", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<UserExercise> userExercises;

    public Workout() {
    }

    public Long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public String getNote() {
        return note;
    }

    public User getUser() {
        return user;
    }

    public List<UserExercise> getUserExercises() {
        return userExercises;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setUserExercises(List<UserExercise> userExercises) {
        this.userExercises = userExercises;
    }

    public void addUserExercise(UserExercise userExercise) {
        userExercises.add(userExercise);
        userExercise.setWorkout(this);
    }

    public void removeUserExercise(UserExercise userExercise) {
        userExercises.remove(userExercise);
        userExercise.setWorkout(null);
    }

    public void setUserExercisesReferences() {
        for(UserExercise userExercise : userExercises) {
            userExercise.setWorkout(this);
            userExercise.setSetsReferences();
        }
    }
}

package com.gainIt.restapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

@Entity
@Table(name = "sets")
public class Set {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    private int reps;
    private float weight;
    private String technique;
    private String note;
    private boolean isDropset;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userExerciseID")
    private UserExercise userExercise;

    public Set() {
    }

    public Set(int reps, float weight, String technique, String note, boolean isDropset, UserExercise userExercise) {
        this.reps = reps;
        this.weight = weight;
        this.technique = technique;
        this.note = note;
        this.isDropset = isDropset;
        this.userExercise = userExercise;
    }

    public Long getId() {
        return id;
    }

    public int getReps() {
        return reps;
    }

    public float getWeight() {
        return weight;
    }

    public String getTechnique() {
        return technique;
    }

    public String getNote() {
        return note;
    }

    public boolean isDropset() {
        return isDropset;
    }

    public UserExercise getUserExercise() {
        return userExercise;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setReps(int reps) {
        this.reps = reps;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public void setTechnique(String technique) {
        this.technique = technique;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setDropset(boolean dropset) {
        isDropset = dropset;
    }

    public void setUserExercise(UserExercise userExercise) {
        this.userExercise = userExercise;
    }
}

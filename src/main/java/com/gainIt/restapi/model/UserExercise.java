package com.gainIt.restapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "userexercises")
public class UserExercise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    private String name;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trainingPlanDayID")
    private TrainingDay trainingDay;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "workoutID")
    private Workout workout;

    @OneToMany(mappedBy = "userExercise", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Set> sets = new ArrayList<>();


    public UserExercise() {
    }

    public UserExercise(TrainingDay trainingDay, List<Set> sets) {
        this.trainingDay = trainingDay;
        this.sets = sets;
    }

    public UserExercise(Workout workout, List<Set> sets) {
        this.workout = workout;
        this.sets = sets;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public TrainingDay getTrainingDay() {
        return trainingDay;
    }

    public Workout getWorkout() {
        return workout;
    }

    public List<Set> getSets() {
        return sets;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTrainingDay(TrainingDay trainingDay) {
        this.trainingDay = trainingDay;
    }

    public void setWorkout(Workout workout) {
        this.workout = workout;
    }

    public void setSets(List<Set> sets) {
        this.sets = sets;
    }

    public void addSet(Set set) {
        sets.add(set);
        set.setUserExercise(this);
    }

    public void removeSet(Set set) {
        sets.remove(set);
        set.setUserExercise(null);
    }

    public void setSetsReferences() {
        for(Set set : sets) {
            set.setUserExercise(this);
        }
    }
}

package com.gainIt.restapi.model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

@Entity
@Table(name="users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    private LoginData loginData;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TrainingPlan> trainingPlans = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Workout> workouts = new ArrayList<>();

    public User() {
    }

    public void setLoginData(LoginData loginData) {
        if (loginData == null) {
            if (this.loginData != null) {
                this.loginData.setUser(null);
            }
        }
        else {
            loginData.setUser(this);
        }
        this.loginData = loginData;
    }

    public LoginData getLoginData() {
        return loginData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<TrainingPlan> getTrainingPlans() {
        return trainingPlans;
    }

    public void setTrainingPlans(List<TrainingPlan> trainingPlans) {
        this.trainingPlans = trainingPlans;
    }

    public List<Workout> getWorkouts() {
        return workouts;
    }

    public void setWorkouts(List<Workout> workouts) {
        this.workouts = workouts;
    }

    public void addTrainingPlan(TrainingPlan trainingPlan) {
        trainingPlan.setTrainingDaysReferences();
        trainingPlans.add(trainingPlan);
        trainingPlan.setUser(this);
    }

    public void removeTrainingPlan(TrainingPlan trainingPlan) {
        trainingPlans.remove(trainingPlan);
        trainingPlan.setUser(null);
    }

    public void updateTrainingPlan(TrainingPlan trainingPlan) {

        ListIterator<TrainingPlan> iterator = trainingPlans.listIterator();
        while (iterator.hasNext()) {
            TrainingPlan plan = iterator.next();
            if (plan.getId().equals(trainingPlan.getId())) {
                plan = trainingPlan;
                iterator.set(plan);
            }
        }
    }

    public void addWorkout(Workout workout) {
        workout.setUserExercisesReferences();
        workouts.add(workout);
        workout.setUser(this);
    }

    public void removeWorkout(Workout workout) {
        workouts.remove(workout);
        workout.setUser(null);
    }
}
